function filterCarByAvailability(cars) {
 
  // Tempat penampungan hasil
  const arr = [];

  // Tulis code-mu disini
  // looping index
  for(let i = 0; i < cars.length; i++) {
    // mengambil data (available true)
    if (cars[i].available == true) {
      arr.push(cars[i]);
    }
  }

  // Rubah code ini dengan array hasil filter berdasarkan availablity
  return arr;
}

console.log(filterCarByAvailability(cars));
