function sortCarByYearAscendingly(cars) {

  // Clone array untuk menghindari side-effect
  // Apa itu side effect?
  const arr = [...cars];
  
  // Tulis code-mu disini
  let penampung = 0;
  // looping index
  for(let i = 0; i < arr.length; i++) {
    for(let j = 0; j < arr.length - i - 1; j++) {
      // mengambil data tahun
      if(arr[j].year > arr[j + 1].year){
        penampung = arr[j];
        arr[j] = arr[j + 1];
        arr[j + 1] = penampung;
      }
    }
  };

  // Rubah code ini dengan array hasil sorting secara ascending
  return arr;
}

console.log(sortCarByYearAscendingly);
